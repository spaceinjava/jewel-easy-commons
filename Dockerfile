FROM java:8
WORKDIR /
ADD target/jewel-easy-commons*.jar jewel-easy-commons.jar
EXPOSE 80
CMD ["java", "-jar", "jewel-easy-commons.jar"]