package com.spaceinje.commons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EnableConfigServer
@SpringBootApplication
@EnableWebSecurity
public class JewelEasyCommonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JewelEasyCommonsApplication.class, args);
	}

}
